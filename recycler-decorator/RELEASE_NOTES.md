# Recycler-decorator Release Notes

- [1.1.0-alpha](#110-alpha)
- [1.0.0](#100)

## 1.1.0-alpha
##### Recycler-decorator
* TODO
## 1.0.0
##### Recycler-decorator
* ANDDEP-785 added Builder for RecyclerView.ItemDecoration and sample
* ANDDEP-785 added ktx for Decorator.Builder to use with EasyAdapter
* Add .gitignore
* fix gradle config for recycler-decorator-easyadapter
* Fix readme
* ANDDEP-1048 Fixing wrong docs links and docs structure